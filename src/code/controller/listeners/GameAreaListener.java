package code.controller.listeners;

import javax.swing.*;

public interface GameAreaListener{
    void useUpdatedState(Icon[][] updatedState);
}
