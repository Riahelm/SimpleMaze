package code.controller.listeners;

import code.controller.PageToShow;

public interface MainFrameListener {
    void onNewPage(PageToShow s);
}
