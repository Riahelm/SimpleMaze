package code.controller;

public enum PageToShow {
    MENU,
    INSTRUCTIONS,
    GAME
}
